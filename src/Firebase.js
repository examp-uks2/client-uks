// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getStorage } from "firebase/storage";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBi2WOIBQVwet5Gl8kDdnkkydgbhRds-U4",
  authDomain: "mini-project-18085.firebaseapp.com",
  projectId: "mini-project-18085",
  storageBucket: "mini-project-18085.appspot.com",
  messagingSenderId: "372179154276",
  appId: "1:372179154276:web:7014506e30593ce1463671",
  measurementId: "G-6GSBLZ0PDH"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);
const analytics = getAnalytics(app);


// // Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
// // TODO: Add SDKs for Firebase products that you want to use
// // https://firebase.google.com/docs/web/setup#available-libraries

// // Your web app's Firebase configuration
// // For Firebase JS SDK v7.20.0 and later, measurementId is optional
// const firebaseConfig = {
//   apiKey: "AIzaSyAFDcNNsmCF-jGMQVzbZMhYrzZdE9HiQoo",
//   authDomain: "movie-client-aditya.firebaseapp.com",
//   projectId: "movie-client-aditya",
//   storageBucket: "movie-client-aditya.appspot.com",
//   messagingSenderId: "224283728841",
//   appId: "1:224283728841:web:9df6797cdfb116d3806618",
//   measurementId: "G-GC4WVH1X5Y"
// };

// // Initialize Firebase
// const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);