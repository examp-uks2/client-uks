import React from "react";
import Sidebar from "../component/Sidebar";
import Profile from "../component/Profile";

function Dashboard() {
  return (
    <div>
      <Sidebar />
      <Profile />


        <div className="main row " style={{
          marginLeft: "20%"
        }}>
          <div
            className="card shadow my-4 col"
            style={{ width: "320px", height: "100px" }}
          >
            <div className="card-body">
              <h5 className="card-title fs-6 text-center">
                Daftar Pasien Guru
              </h5>
              <h6
                className="card-subtitle text-center fs-2 mb-2"
                style={{ color: "#35e97a" }}
              >
                <span className="fa-stack fa-xs">
                  <i className="fa fa-circle fa-stack-2x"></i>
                  <i className="fa fa-solid fa-wheelchair fa-stack-1x fa-inverse"></i>
                </span>
                0 Guru
              </h6>
            </div>
          </div>
          <div className="card shadow m-4 col" style={{ height: "100px" }}>
            <div className="card-body">
              <h5 className="card-title fs-6 text-center">
                Daftar Pasien Siswa
              </h5>
              <h6
                className="card-subtitle text-center fs-2 mb-2"
                style={{ color: "#35e97a" }}
              >
                <span className="fa-stack fa-xs">
                  <i className="fa fa-circle fa-stack-2x"></i>
                  <i className="fa fa-solid fa-wheelchair fa-stack-1x fa-inverse"></i>
                </span>
                0 Siswa
              </h6>
            </div>
          </div>
          <div
            className="card shadow m-4 col"
            style={{ width: "340px", height: "100px" }}
          >
            <div className="card-body">
              <h5 className="card-title fs-6 text-center">
                Daftar Pasien Karyawan
              </h5>
              <h6
                className="card-subtitle text-center fs-2 mb-2"
                style={{ color: "#35e97a" }}
              >
                <span className="fa-stack fa-xs">
                  <i className="fa fa-circle fa-stack-2x"></i>
                  <i className="fa fa-solid fa-wheelchair fa-stack-1x fa-inverse"></i>
                </span>
                0 Karyawan
              </h6>
            </div>
          </div>

        </div>
        {/* TABLE */}
        <div
          className="card shadow"
          style={{
            marginRight: "%",
            marginLeft: "21%",
            borderRadius: "10px",
            backgroundColor: "#00f79c",
          }}
        >
          <p className="gren text-center text-white fs-4">Riwayat Pasien</p>
          <table className="table">
            <thead>
              <tr className="table-light">
                <th scope="col" className="table-light">
                  No
                </th>
                <th scope="col">Nama Pasien</th>
                <th scope="col">Status Pasien</th>
                <th scope="col">Tanggal/Jam Periksa</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
      </div>
    </div>
  );
}

export default Dashboard;
