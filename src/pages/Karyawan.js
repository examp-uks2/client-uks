import React, { useState, useEffect } from "react";
import { instance as axios } from "../utils/Api.js";
// import axios from "axios";
import { useNavigate, Link, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import Sidebar from "../component/Sidebar.js";
import Profile from "../component/Profile.js";
import ReactHTMLTableToExcel from "react-html-table-to-excel";

// Tambah

function Karyawan() {
  const navigate = useNavigate();
  const { id } = useParams();
  const [nama_karyawan, setNama_karyawan] = useState("");
  const [tempat_tanggal_lahir, setTempat_tanggal_lahir] = useState("");
  const [alamat, setAlamat] = useState("");
  const [employee, setEmployee] = useState([]);

  const pasienKaryawan = async () => {
    try {
      const formData = {
        nama_karyawan: nama_karyawan,
        tempat_tanggal_lahir: tempat_tanggal_lahir,
        alamat: alamat,
      };
      await axios.post(`/karyawan/add`, formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
    } catch (err) {
      console.log(err);
    }
    navigate("/karyawan");
    Swal.fire({
      icon: "success",
      title: "Tambah Pasien Success",
      showConfirmButton: false,
      timer: 800,
    });
  };

  const submit = () => {};

  const save = (e) => {
    e.preventDefault();
    submit();
  };

  // GET ALL
  const fetchEmployee = async () => {
    try {
      const { data, status } = await axios.get(
        `http://localhost:8080/pasien/karyawan/all`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      // if (status === 200) {
      setEmployee(data);
      // }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchEmployee();
  }, []);

  // REMOVE
  const removePost = async (id) => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Yes, delete it!",
      denyButtonText: "Cancel",
    }).then((result) => {
      if (result.isConfirmed) {
        navigate("/karyawan");
        axios.delete(`karyawan/${id}`, {});
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };
  return (
    <div>
      <Sidebar />
      <Profile />
      {/* MODAL */}

      <div className="d-flex justify-content-between">
        <span></span>

        {/* modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title" id="exampleModalLabel">
                  Tambah Karyawan
                </h4>
                <button
                  type="button"
                  className="close btn btn-danger"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form onSubmit={save}>
                  <div className="form-group">
                    <label>Nama karyawan</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="..."
                      value={nama_karyawan}
                      onChange={(e) => setNama_karyawan(e.target.value)}
                    />
                    <label>Tempat Tanggal Lahir</label>
                    <input
                      type="date"
                      className="form-control"
                      placeholder="..."
                      value={tempat_tanggal_lahir}
                      onChange={(e) => setTempat_tanggal_lahir(e.target.value)}
                    />
                    <label>Alamat</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="..."
                      value={alamat}
                      onChange={(e) => setAlamat(e.target.value)}
                    />
                  </div>
                  <div className="modal-footer">
                    <button
                      onClick={pasienKaryawan}
                      type="button submit"
                      className="btn btn-success"
                    >
                      Tambah
                    </button>
                    <button
                      type="button"
                      className="btn btn-danger"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      Close
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        {/* modal */}

        <div
          className="card shadow"
          style={{
            marginRight: "43px",
            margin: "43px",
            borderRadius: "10px",
            backgroundColor: "#00f79c",
            width: "75%",
          }}
        >
          <p className="gren text-center text-white fs-4">Riwayat Pasien</p>
          <div
            className="btn-group"
            role="group"
            aria-label="Basic outlined example"
          >
            <button
              type="button"
              data-toggle="modal"
              data-target="#exampleModal"
              className="btn btn-outline-success"
            >
              <i className="fa-sharp fa-solid fa-plus"></i> Tambah
            </button>
            <button
              type="button"
              data-bs-toggle="modal"
              data-bs-target="#exampleModal1"
              className="btn btn-outline-success"
            >
              Import
            </button>
            <ReactHTMLTableToExcel
              id="test-table-xls-button"
              table="table-to-xls"
              filename="tablexls"
              sheet="tablexls"
              className="btn btn-outline-success"
            ></ReactHTMLTableToExcel>
          </div>
          <table className="table" id="table-to-xls">
            <thead>
              <tr className="table-light">
                <th scope="col" className="table-light">
                  No
                </th>
                <th scope="col">Nama Karyawan</th>
                <th scope="col">Tempat/Tanggal/Lahir</th>
                <th scope="col">Alamat</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
              {employee.map((karyawan, index) => (
                <tr key={index} className="text-dark bg-light">
                  <th scope="row">{karyawan.id}</th>
                  <td>{karyawan.nama_karyawan}</td>
                  <td>{karyawan.tempat_tanggal_lahir}</td>
                  <td>{karyawan.alamat}</td>
                  <td>
                    <div
                      onClick={() => removePost(karyawan.id)}
                      className="btn btn-danger text-btn text-light "
                    >
                      <i className="fa-solid fa-trash"></i>
                    </div>
                    <Link
                      to={`/editkaryawan/${karyawan.id}`}
                      className="btn btn-info   text-btn text-light mx-3"
                    >
                      <i className="fa-solid fa-pen-to-square"></i>
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default Karyawan;
