import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";

function Register() {
  const location = useLocation();
  const navigate = useNavigate();

  const [userRegister, setUserRegister] = useState({
    username: "",
    password: "",
  });

  const handleOnChange = (e) => {
    setUserRegister((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  const register = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`http://localhost:8080/register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(userRegister),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem("token", data.token);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          navigate("/login");
        }
      }
    } catch (err) {
      console.log(err);
    }
    
  };

  return (
    <div className="text-black">
      <div className="container mt-5 p-3 card" style={{width: "400px", height: "450px"}}>
        <div className="auth-form-container p-5">
          <h2>Register</h2>
          <form className="register-form">
            <label htmlFor="email" className="text-black">username</label>
            <input
              value={userRegister.username}
              onChange={handleOnChange}
              type="username"
              placeholder="yourname"
              id="username"
              name="username"
            />
            <label htmlFor="password" className="text-black">password</label>
            <input
              value={userRegister.password}
              onChange={handleOnChange}
              type="password"
              placeholder="********"
              id="password"
              name="password"
            />
          </form>
          <div className="text-center mt-4">
            <button
              type="button"
              className="btn btn-outline-success btn-color zoom mb-2 2-100"
              onClick={register}
            >
              Register
            </button>
            <p className="small fw-bold mt-2 mb-0">
              Sudah Punya Akun? 
              <Link className="link-success" to="/login">
                Login
              </Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;
