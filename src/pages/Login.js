import React, { useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import Swal from "sweetalert2";
import "../css/login.css";

function Login() {
  const location = useLocation();
  const navigate = useNavigate();

  const [userLogin, setUserLogin] = useState({
    username: "",
    password: "",
  });

  const handleOnChange = (e) => {
    setUserLogin((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  const signIn = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`http://localhost:8080/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(userLogin),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem("token", data.token);
        localStorage.setItem("id", data.userData.id);
        // console.log(data.token);
        // console.log(data.userData.id);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          navigate("/dashboard");
        }
      }
    } catch (err) {
      console.log(err);
    }
    Swal.fire({
      icon: "success",
      title: "Login Success",
      showConfirmButton: false,
      timer: 800,
    });
  };

  return (
    <div className="text-black">
      <h2 className="text-center m-3">Sistem Aplikasi UKS</h2>
      <img
        className="rounded mx-auto d-block m-3"
        style={{ width: "12%", height: "12%" }}
        src="http://103.133.27.106:3000/static/media/smpn1smg.174af4089a0f0045f9a4.png"
      />
      <div
        className="container shadow text-white card"
        style={{ width: "400px", height: "400px" }}
      >
        <div className="auth-form-container text-black p-5">
          <h4 className="text-center">
            <b>Login</b>
          </h4>
          <form className="register-form">
            <label htmlFor="email" className="text-black">
              <b>Username</b>
            </label>
            <input
              value={userLogin.username}
              onChange={handleOnChange}
              type="username"
              placeholder="yourname"
              id="username"
              name="username"
            />
            <label htmlFor="password" className="text-black">
              <b>Password</b>
            </label>
            <input
              value={userLogin.password}
              onChange={handleOnChange}
              type="password"
              placeholder="********"
              id="password"
              name="password"
            />
          </form>
          <div className="text-center mt-4">
            <button
              type="button"
              className="btn text-white zoom px-2"
              style={{ backgroundColor: "#18cc4e", width: "100%" }}
              onClick={signIn}
            >
              <b>LogIn</b>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Login;
