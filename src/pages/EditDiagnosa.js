import React from "react";
import { instance as axios } from "../utils/Api";

import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

function EditDiagnosa() {
  const navigate = useNavigate();
  const { id } = useParams();
  const [nama_diagnosa, setNama_diagnosa] = useState("");

 
  // UPDATE
  const updateDiagnosa = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const data = {
        nama_diagnosa,
      };

      Swal.fire({
        title: "Do you want to save changes?",
        icon: "question",
        showDenyButton: true,
        confirmButtonText: "Save",
        denyButtonText: `Cancel`,
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(data);
          axios.put(`diagnosa/${id}`, data);

          navigate("/diagnosa");
          Swal.fire({
            icon: "success",
            title: "Saved!",
            text: "Pasien changed successfully!",
            showConfirmButton: false,
            timer: 1000,
          });
        } else if (result.isDenied) {
          Swal.fire({
            icon: "info",
            title: "",
            text: "Changes are not saved!",
            showConfirmButton: false,
            timer: 1000,
          });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
  const save = () => {};


  const getById = async () => {
    const { data } = await axios.get(`diagnosa/id/${id}`);

    setNama_diagnosa(data.nama_diagnosa);
  };

  useEffect(() => {
    getById();
  }, [id]);

  return (
    <div className="container card shadow" style={{ marginLeft: "24%",width: "70%", marginTop: "150px" }}>
      <form className="">
        <div className="row g-3 mb-md-2 mt-md-2 pb-3">
          <div className="col form-outline form-white mb-4">
            <input
              type="text"
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Title"
              value={nama_diagnosa}
              onChange={(e) => setNama_diagnosa(e.target.value)}
            />
          </div>
        </div>
      </form>
      <div className="col-12 ">
        <button
          type="submit"
          onClick={updateDiagnosa}
          className="btn btn-success zoom text-white my-4"
        >
          Save
        </button>
      </div>
    </div>
  );
}

export default EditDiagnosa;
