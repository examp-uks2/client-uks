import React from "react";
import { instance as axios } from "../utils/Api";

import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

function EditSiswa() {
  const navigate = useNavigate();
  const { id } = useParams();
  const [nama_siswa, setNama_siswa] = useState("");
  const [kelas, setKelas] = useState("");
  const [tempat_tanggal_lahir, setTempat_tanggal_lahir] = useState("");
  const [alamat, setAlamat] = useState("");

 
  // UPDATE
  const updateSiswa = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const data = {
        nama_siswa,
        kelas,
        tempat_tanggal_lahir,
        alamat,
      };

      Swal.fire({
        title: "Do you want to save changes?",
        icon: "question",
        showDenyButton: true,
        confirmButtonText: "Save",
        denyButtonText: `Cancel`,
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(data);
          axios.put(`siswa/${id}`, data);

          navigate("/siswa");
          Swal.fire({
            icon: "success",
            title: "Saved!",
            text: "Pasien changed successfully!",
            showConfirmButton: false,
            timer: 1000,
          });
        } else if (result.isDenied) {
          Swal.fire({
            icon: "info",
            title: "",
            text: "Changes are not saved!",
            showConfirmButton: false,
            timer: 1000,
          });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
  const save = () => {};


  const getById = async () => {
    const { data } = await axios.get(`siswa/id/${id}`);

    setNama_siswa(data.nama_siswa);
    setKelas(data.kelas);
    setTempat_tanggal_lahir(data.tempat_tanggal_lahir);
    setAlamat(data.alamat);
  };

  useEffect(() => {
    getById();
  }, [id]);

  return (
    <div className="container card shadow" style={{ marginLeft: "24%",width: "70%", marginTop: "150px" }}>
      <form className="">
        <div className="row g-3 mb-md-2 mt-md-2 pb-3">
          <div className="col form-outline form-white mb-4">
            <input
              type="text"
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Title"
              value={nama_siswa}
              onChange={(e) => setNama_siswa(e.target.value)}
            />
          </div>
          <div className="col form-outline form-white mb-4">
            <input
              type="text"
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Title"
              value={kelas}
              onChange={(e) => setKelas(e.target.value)}
            />
          </div>
          <div className="col form-outline form-white mb-4">
            <input
              type="date"
              placeholder="..."
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              value={tempat_tanggal_lahir}
              onChange={(e) => setTempat_tanggal_lahir(e.target.value)}
            />
          </div>
          <div className="form-outline form-white mb-4">
            <input
              type="text"
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Alamat"
              value={alamat}
              onChange={(e) => setAlamat(e.target.value)}
            />
          </div>
        </div>
      </form>
      <div className="col-12 ">
        <button
          type="submit"
          onClick={updateSiswa}
          className="btn btn-success zoom text-white my-4"
        >
          Save
        </button>
      </div>
    </div>
  );
}

export default EditSiswa;
