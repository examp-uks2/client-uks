import React, { useState, useEffect } from "react";
import { instance as axios } from "../utils/Api.js";
// import axios from "axios";
import { useNavigate, Link, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import Sidebar from "../component/Sidebar.js";
import Profile from "../component/Profile.js";

// Tambah

function PenangananPertama() {
  const navigate = useNavigate();
  const { id } = useParams();
  const [nama_penanganan, setNama_penanganan] = useState("");
  const [firstHandle, setFirstHandle] = useState([]);

  const pasienPenanganan = async () => {
    try {
      const formData = {
        nama_penanganan: nama_penanganan,
      };
      await axios.post(`/penanganan/add`, formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
    } catch (err) {
      console.log(err);
    }
    navigate("/penanganan");
    Swal.fire({
      icon: "success",
      title: "Berhasil Menambahkan Penanganan",
      showConfirmButton: false,
      timer: 800,
    });
  };

  const submit = () => {};

  const save = (e) => {
    e.preventDefault();
    submit();
  };

  // GET ALL
  const fetchFirstHandle = async () => {
    try {
      const { data, status } = await axios.get(
        `http://localhost:8080/penanganan/all`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      // if (status === 200) {
      setFirstHandle(data);
      // }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchFirstHandle();
  }, []);

  // REMOVE
  const removePost = async (id) => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Yes, delete it!",
      denyButtonText: "Cancel",
    }).then((result) => {
      if (result.isConfirmed) {
        navigate("/penanganan");
        axios.delete(`penanganan/${id}`, {});
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };
  return (
    <div>
      <Sidebar />
      <Profile />
      {/* MODAL */}

      <div className="d-flex justify-content-between">
        <span></span>

        {/* modal */}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title" id="exampleModalLabel">
                  Tambah penanganan
                </h4>
                <button
                  type="button"
                  className="close btn btn-danger"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form onSubmit={save}>
                  <div className="form-group">
                    <label>Nama Penanganan</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="..."
                      value={nama_penanganan}
                      onChange={(e) => setNama_penanganan(e.target.value)}
                    />
                  </div>
                  <div className="modal-footer">
                    <button
                      onClick={pasienPenanganan}
                      type="button submit"
                      className="btn btn-success"
                    >
                      Tambah
                    </button>
                    <button
                      type="button"
                      className="btn btn-danger"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      Close
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        {/* modal */}

        <div
          className="card shadow"
          style={{
            marginRight: "43px",
            margin: "43px",
            borderRadius: "10px",
            backgroundColor: "#00f79c",
            width: "75%",
          }}
        >
          <button
            type="button"
            className="btn btn-success float-left m-2"
            data-toggle="modal"
            data-target="#exampleModal"
          >
            <i className="fa-sharp fa-solid fa-plus"></i> Tambah
          </button>
          <p className="gren text-center text-white fs-4">Penanganan Pertama</p>
          <table className="table">
            <thead>
              <tr className="table-light ">
                <th scope="col" className="table-light">
                  No
                </th>
                <th scope="col" className="text-center">
                  Nama Penanganan
                </th>
                <th scope="col" className="text-end text-center">
                  Aksi
                </th>
              </tr>
            </thead>
            <tbody>
              {firstHandle.map((penanganan, index) => (
                <tr key={index} className="text-dark bg-light">
                  <th scope="row">{penanganan.id}</th>
                  <td className="text-center">{penanganan.nama_penanganan}</td>
                  <td>
                    <div
                      onClick={() => removePost(penanganan.id)}
                      className="btn btn-danger text-btn text-light "
                    >
                      <i className="fa-solid fa-trash"></i>
                    </div>
                    <Link
                      to={`/editpenanganan/${penanganan.id}`}
                      className="btn btn-info   text-btn text-light mx-3"
                    >
                      <i className="fa-solid fa-pen-to-square"></i>
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default PenangananPertama;
