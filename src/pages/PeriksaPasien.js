import React, { useState, useEffect } from "react";
import { instance as axios } from "../utils/Api.js";
// import axios from "axios";
import { useNavigate, Link, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import Sidebar from "../component/Sidebar.js";
import Profile from "../component/Profile.js";

// Tambah

function PeriksaPasien() {
  const navigate = useNavigate();
  const { id } = useParams();
  const [nama_pasien, setNama_pasien] = useState("");
  const [status_pasien, setStatus_pasien] = useState("");
  const [jabatan, setJabatan] = useState("");
  const [tanggal, setTanggal] = useState("");
  const [keterangan, setKeterangan] = useState("");
  const [status, setStatus] = useState("");
  const [periksa, setPeriksa] = useState([]);

  const periksaPasien = async () => {
    try {
      const formData = {
        nama_pasien: nama_pasien,
        status_pasien: status_pasien,
        jabatan: jabatan,
        tanggal: tanggal,
        keterangan: keterangan,
        status: status,
      };
      await axios.post(`/periksaPasien/add`, formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
    } catch (err) {
      console.log(err);
    }
    navigate("/periksaPasien");
    Swal.fire({
      icon: "success",
      title: "Tambah Pasien Success",
      showConfirmButton: false,
      timer: 800,
    });
  };

  const submit = () => {};

  const save = (e) => {
    e.preventDefault();
    submit();
  };

  // GET ALL
  const fetchPeriksaPasien = async () => {
    try {
      const { data, status } = await axios.get(
        `http://localhost:8080/periksaPasien/all`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      // if (status === 200) {
      setPeriksa(data);
      // }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchPeriksaPasien();
  }, []);

  // REMOVE
  const removePost = async (id) => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Yes, delete it!",
      denyButtonText: "Cancel",
    }).then((result) => {
      if (result.isConfirmed) {
        navigate("/periksaPasien");
        axios.delete(`periksaPasien/delete/${id}`, {});
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };
  return (
    <div>
      <Sidebar />
      <Profile />
      {/* MODAL */}

      <div className="d-flex justify-content-between">
        <span></span>

        {/* modal 1*/}
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title" id="exampleModalLabel">
                  Tambah Guru
                </h4>
                <button
                  type="button"
                  className="close btn btn-danger"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form onSubmit={save}>
                  <div className="form-group">
                    <label>Nama Guru</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="..."
                      value={nama_pasien}
                      onChange={(e) => setNama_pasien(e.target.value)}
                    />
                    <label>Tempat Tanggal Lahir</label>
                    <input
                      type="date"
                      className="form-control"
                      placeholder="..."
                      value={status_pasien}
                      onChange={(e) => setStatus_pasien(e.target.value)}
                    />
                    <label>Jabatan</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="..."
                      value={jabatan}
                      onChange={(e) => setJabatan(e.target.value)}
                    />
                    <label>Tnggal/Jam</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="..."
                      value={tanggal}
                      onChange={(e) => setTanggal(e.target.value)}
                    />
                    <label>Keterangan</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="..."
                      value={keterangan}
                      onChange={(e) => setKeterangan(e.target.value)}
                    />
                    <label>Status</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="..."
                      value={status}
                      onChange={(e) => setStatus(e.target.value)}
                    />
                  </div>
                  <div className="modal-footer">
                    <button
                      onClick={periksaPasien}
                      type="button submit"
                      className="btn btn-success"
                    >
                      Tambah
                    </button>
                    <button
                      type="button"
                      className="btn btn-danger"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      Close
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        {/* modal 2*/}
        <div
          className="modal fade"
          id="exampleModal1"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h1 className="modal-title fs-5" id="exampleModalLabel">
                  Modal title
                </h1>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close2"
                ></button>
              </div>
              <div className="modal-body"></div>
              <button
                type="button"
                className="btn btn-danger"
                data-bs-dismiss="modal"
                aria-label="Close1"
              >
                Close
              </button>
            </div>
          </div>
        </div>

        {/* button modal */}

        <div
          className="card shadow"
          style={{
            marginRight: "43px",
            margin: "43px",
            borderRadius: "10px",
            backgroundColor: "#00f79c",
            width: "75%",
          }}
        >
          <button
            type="button"
            data-toggle="modal"
            data-target="#exampleModal"
            className="btn btn-outline-success"
          >
            <i className="fa-sharp fa-solid fa-plus"></i> Tambah
          </button>
          {/* <button
            type="button"
            className="btn btn-success float-left m-2"
            data-toggle="modal"
            data-target="#exampleModal"
          >
            <i className="fa-sharp fa-solid fa-plus"></i> Tambah
          </button> */}
          <p className="gren text-center text-white fs-4">Periksa Pasien</p>
          <table className="table">
            <thead>
              <tr className="table-light">
                <th scope="col" className="table-light">
                  No
                </th>
                <th scope="col">Nama Pasien</th>
                <th scope="col">Status Pasien</th>
                <th scope="col">Jabatan</th>
                <th scope="col">Tanggal/Jam</th>
                <th scope="col">Keterangan</th>
                <th scope="col">Status</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
              {periksa.map((pasien, index) => (
                <tr key={index} className="text-dark bg-light">
                  <th scope="row">{pasien.id}</th>
                  <td>{pasien.nama_pasien}</td>
                  <td>{pasien.status_pasien}</td>
                  <td>{pasien.jabatan}</td>
                  <td>{pasien.tanggal}</td>
                  <td>{pasien.keterangan}</td>
                  <td>{pasien.status}</td>
                  <td>
                    <div
                      onClick={() => removePost(pasien.id)}
                      className="btn btn-danger   text-btn text-light "
                    >
                      <i className="fa-solid fa-trash"></i>
                    </div>
                    <Link
                      to={`/periksaPasien/${pasien.id}`}
                      className="btn btn-info   text-btn text-light mx-3"
                    >
                      <i className="fa-solid fa-pen-to-square"></i>
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default PeriksaPasien;
