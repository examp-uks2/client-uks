import React from "react";
import { Link } from "react-router-dom";
import '../css/profile.css'

function Profile() {
  return (
    <div>
      <nav className="navbar bg-success" style={{ marginLeft: "19%" }}>
        <div className="container-fluid">
          <div class="d-flex text-white justify-content-end">
            <h4>UKS</h4>
          </div>
          <Link
            className="navbar-brand img1"
            to="/profile"
          >
            <img
              src="https://www.w3schools.com/howto/img_avatar.png"
              alt="Avatar"
              width="50"
              height="50"
              style={{ borderRadius: "50%", float: "right" }}
              className="d-inline-block float-end align-end d-flex justify-content-end"
            />
          </Link>
        </div>
      </nav>
    </div>
  );
}

export default Profile;
