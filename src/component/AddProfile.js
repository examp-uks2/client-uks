import React, { useEffect, useState } from "react";
import { instance as axios } from "../utils/Api";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { storage } from "../Firebase";

function AddProfile() {
    const navigate = useNavigate();
  const [image, setImage] = useState(null);
  const [nama, setNama] = useState("");
  const [kelas, setKelas] = useState("");
  const [sekolah, setSekolah] = useState("");
  const [lahir, setLahir] = useState("");

  const addProfile = async (downloadURL) => {
    try {
      const formData = {
        image: downloadURL,
        nama: nama,
        kelas: kelas,
        sekolah: sekolah,
        lahir: lahir,
      };
      await axios.post(`profile/add`, formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
    } catch (err) {
      console.log(err);
    }
    navigate("/dashboard");
    Swal.fire({
      icon: "success",
      title: "Berhasil Menambahkan Profile",
      showConfirmButton: false,
      timer: 800,
    });
  };


  const submit = () => {
    // untuk storage
    const storageRef = ref(storage, `images/${image.name}`);
    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadURL) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          addProfile(downloadURL);
          console.log(downloadURL);
        });
      });
  };

  const save = (e) => {
    e.preventDefault();
    submit();
  };
  // // REMOVE
  // const removePost = async (id) => {
  //   await Swal.fire({
  //     title: "Do you want to delete the Post?",
  //     icon: "warning",
  //     showDenyButton: true,
  //     confirmButtonText: "Yes, delete it!",
  //     denyButtonText: "Cancel",
  //   }).then((result) => {
  //     if (result.isConfirmed) {
  //       navigate("/guru");
  //       axios.delete(`guru/delete/${id}`, {});
  //       Swal.fire({
  //         icon: "success",
  //         title: "Deleted!",
  //         text: "Successfully deleted your post!",
  //         showConfirmButton: false,
  //         timer: 1000,
  //       });
  //     } else if (result.isDenied) {
  //       Swal.fire({
  //         icon: "error",
  //         title: "Canceled",
  //         text: "",
  //         showConfirmButton: false,
  //         timer: 1000,
  //       });
  //     }
  //   });
  // };
  return (
    <div>AddProfile</div>
  )
}

export default AddProfile