import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useNavigate, useLocation } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-icons/font/bootstrap-icons.json";
import "bootstrap/js/dist/dropdown";
import "bootstrap/js/dist/collapse";
import "../css/sidebar.css";

function Sidebar() {
  const navigate = useNavigate();
  const [clockState, setClockState] = useState();

  const logout = () => {
    localStorage.removeItem("token");
    navigate("/");
  };

  useEffect(() => {
    setInterval(() => {
      const date = new Date();
      setClockState(date.toLocaleTimeString());
    }, 1000);
  }, []);
  const now = new Date();
  const formattedDate = now.toLocaleDateString();
  return (
    <div><b>
      <div className="sidebar">
        <h5 className="text-light text-center mt-3">
          SISTEM APLIKASI UKS <br /> SMPN 1 SEMARANG
        </h5>
        <hr />
        <Link to={"/dashboard"}>
          <i className="fa fa-fw fa-home"></i> Dashboard
        </Link>
        <Link to={"/periksaPasien"}>
          <i className="fa-solid fa-stethoscope"></i> Periksa Pasien
        </Link>
        {/* <Link to={"/"}> 
          <i 
            className="fa-solid fa-arrow-down fa-lg" 
            style={{ marginLeft: "3px" }} 
          ></i> 
          Data 
        </Link> */}
        
        <Link to={"/guru"}>
          <i className="fa-solid fa-graduation-cap"></i> Data Guru
        </Link>
        <Link to={"/siswa"}>
          <i className="fa-solid fa-user" style={{ marginLeft: "5px" }}></i>{" "}
          Data Siswa
        </Link>
        <Link to={"/karyawan"}>
          <i className="fa-solid fa-users" style={{ marginLeft: "5px" }}></i>{" "}
          Data Karyawan
        </Link>
        <Link to={"/diagnosa"}>
          <i className="fa-solid fa-person-dots-from-line"></i> Diagnosa
        </Link>
        <Link to={"/penanganan"}>
          <i className="fa-solid fa-hands-holding"></i> Penanganan Pertama
        </Link>
        <Link to={"/tindakan"}>
          <i className="fa-solid fa-location-crosshairs"></i> Tindakan
        </Link>
        <Link to={"/obat"}>
          <i className="fa-solid fa-capsules"></i> Daftar Obat P3K
        </Link>
        <div className="my-4">
          <i>
            <b>
              <div className="text-white text-center fs-5 ">
                {formattedDate}
              </div>
              <div className="text-white text-center fs-5 ">{clockState}</div>
            </b>
          </i>
        </div>
        <Link type="button" className=" btn-light" onClick={logout}>
        <i class="fa-solid fa-right-from-bracket"></i> Logout
        </Link>
      </div>
      </b>
    </div>
  );
}

export default Sidebar;
