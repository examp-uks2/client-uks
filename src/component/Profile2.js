import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { instance as axios } from "../utils/Api";

function Profile2() {
  const [profile, setProfile] = useState([]);

  const fetchProfile = async () => {
    try {
      const { data, status } = await axios.get(`profile/all`, {});
      if (status === 200) {
        setProfile(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchProfile();
  }, []);
  return (
    <div className="">
      <div className="card container p-5 shadow" style={{ marginTop: "10%" }}>
        <div className="card-header bg-secondary">
          <div style={{ border: "2px solid white", borderRadius: "20px" }}>
            <img
              src="https://www.w3schools.com/howto/img_avatar.png"
              width="25%"
              height="25%"
              className="m-2"
              style={{ borderRadius: "50%" }}
            />

            <div className="btn text-white btn-dark">
              <i className="fa-solid fa-plus"></i>
            </div>

            <div className="text-white d-inline-flex p-2">
              <p className="col text-center">
                Nama
                <h4>Aditya Muhammad K</h4>
              </p>

              <p className="col text-center">
                Kelas
                <h4>10</h4>
              </p>
              <p className="col text-center">
                Sekolah
                <h4>SMK Bina Nusantara</h4>
              </p>
              <p className="col text-center">
                Lahir
                <h4>19-12-2006</h4>
              </p>
            </div>
          </div>
        </div>
        <div className="card-body" style={{ backgroundColor: "aquamarine" }}>
          <blockquote className="blockquote mb-0">
            <div className="float-end">
              <Link className="btn btn-outline-light text-dark m-3">
                Edit Profile <i class="fa-solid fa-pen-to-square"></i>
              </Link>
              <Link className="btn btn-outline-light text-dark m-3">
                Edit Password <i class="fa-solid fa-pen-to-square"></i>
              </Link>
            </div>
            <Link to="/dashboard" className="btn">
              <footer className="blockquote-footer text-middle">Profile</footer>
            </Link>
          </blockquote>
        </div>
      </div>
    </div>
  );
}

export default Profile2;
