import { Route, Routes } from "react-router-dom";
// import Sidebar from "./component/Sidebar";
import Dashboard from "./pages/Dashboard";
import Login from "./pages/Login";
import Register from "./pages/Register";
import PeriksaPasien from "./pages/PeriksaPasien";
import Guru from "./pages/Guru";
import Siswa from "./pages/Siswa";
import PenangananPertama from "./pages/PenangananPertama";
import Tindakan from "./pages/Tindakan";
import DaftarObat from "./pages/daftar-obat";
import Diagnosa from "./pages/Diagnosa";
import Sidebar from "./component/Sidebar";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-icons/font/bootstrap-icons.json";
import "bootstrap/js/dist/dropdown";
import "bootstrap/js/dist/collapse";
import PrivateRoute from "./component/PrivateRoute";
// import Hello from "./pages/Hello";
import EditGuru from "./pages/EditGuru";
import EditSiswa from "./pages/EditSiswa";
import Karyawan from "./pages/Karyawan";
import EditKaryawan from "./pages/EditKaryawan";
import EditDiagnosa from "./pages/EditDiagnosa";
import Profile2 from "./component/Profile2";
import AddProfile from "./component/AddProfile";

// import Hello from "./pages/Hello";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Login />}>
          Login
        </Route>
        <Route path="/register" element={<Register />}>
          Register
        </Route>
        {/* <Route path="/hello" element={<Hello />} /> */}
        <Route
          path="/dashboard"
          element={
            <PrivateRoute>
              <Dashboard />
            </PrivateRoute>
          }
        />
        <Route
          path="/profile"
          element={
            <PrivateRoute>
              <Profile2 />
            </PrivateRoute>
          }
        />
        <Route
          path="/addprofile"
          element={
            <PrivateRoute>
              <AddProfile />
            </PrivateRoute>
          }
        />
        <Route
          path="/periksaPasien"
          element={
            <PrivateRoute>
              <PeriksaPasien />
            </PrivateRoute>
          }
        />
        <Route
          path="/guru"
          element={
            <PrivateRoute>
              <Guru />
            </PrivateRoute>
          }
        />
        <Route
          path="/editguru/:id"
          element={
            <PrivateRoute>
              <EditGuru />
            </PrivateRoute>
          }
        />
        <Route
          path="/siswa"
          element={
            <PrivateRoute>
              <Siswa />
            </PrivateRoute>
          }
        />
        <Route
          path="/editsiswa/:id"
          element={
            <PrivateRoute>
              <EditSiswa />
            </PrivateRoute>
          }
        />
        <Route
          path="/karyawan"
          element={
            <PrivateRoute>
              <Karyawan />
            </PrivateRoute>
          }
        />
        <Route
          path="/editkaryawan/:id"
          element={
            <PrivateRoute>
              <EditKaryawan />
            </PrivateRoute>
          }
        />
        <Route
          path="/diagnosa"
          element={
            <PrivateRoute>
              <Diagnosa />
            </PrivateRoute>
          }
        />
        <Route
          path="/editdiagnosa/:id"
          element={
            <PrivateRoute>
              <EditDiagnosa />
            </PrivateRoute>
          }
        />
        <Route
          path="/penanganan"
          element={
            <PrivateRoute>
              <PenangananPertama />
            </PrivateRoute>
          }
        />
        <Route
          path="/tindakan"
          element={
            <PrivateRoute>
              <Tindakan />
            </PrivateRoute>
          }
        />
        <Route
          path="/obat"
          element={
            <PrivateRoute>
              <DaftarObat />
            </PrivateRoute>
          }
        />
      </Routes>
    </div>
  );
}

export default App;
